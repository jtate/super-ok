#! /usr/bin/env node

var fs = require('fs');
var nodegit = require('nodegit');
var chalk = require('chalk');
var path = require('path');
var checkTags = require('./lib/check-tags');
var checkStatuses = require('./lib/check-statuses');

var argv = require('minimist')(process.argv.slice(2));

var dirs = fs.readdirSync('.');

dirs.forEach(function(dir){
  var pathToUse = path.resolve(dir);
  nodegit.Repository.open(pathToUse)
    .then(function (repo) {
      if (argv.tags === true) {
        checkTags(repo, dir);
        return;
      }
      if (argv.status === true){
        checkStatuses(repo, dir);
        return;
      }
      repo.getCurrentBranch().then(function(ref){
        var branchName = ref.shorthand();
        if (branchName !== 'develop') {
          console.log(dir, chalk.grey('is on'), chalk.yellow(branchName));
        } else {
          console.log(dir, chalk.grey('is on'), chalk.green(branchName));
        }
      });
    });
});
