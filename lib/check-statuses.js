var nodegit = require('nodegit');
var chalk = require('chalk');

module.exports = function checkStatuses (repo, dir) {
  repo.getStatus().then(function(statuses) {
    function statusToText(status) {
      var words = [];
      if (status.isNew()) { words.push(chalk.green('NEW')); }
      if (status.isModified()) { words.push(chalk.yellow('MODIFIED')); }
      if (status.isTypechange()) { words.push(chalk.yellow('TYPECHANGE')); }
      if (status.isRenamed()) { words.push(chalk.yellow('RENAMED')); }
      if (status.isIgnored()) { words.push(chalk.grey('IGNORED')); }
      return words.join(' ');
    }
    console.log('for', dir);
    statuses.forEach(function(file) {
      console.log(file.path() + " " + statusToText(file));
    });
  });
};
