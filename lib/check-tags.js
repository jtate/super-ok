var nodegit = require('nodegit');
var chalk = require('chalk');

var pathToSelf = require('path').resolve(__dirname);


module.exports = function checkTags (repo, dir) {
  nodegit.Tag.list(repo).then(function(tags){
    if (tags.length === 0) {
      console.log(chalk.red(dir + ' has no tags!'));
    } else {
      tags.forEach(function(tag){
        if (!tag.startsWith('v')){
          console.log(chalk.red('tag ' + tag + ' for ' + dir + ' is missing the v'));
        }
      });
    }
  });
};
