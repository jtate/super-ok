# super-ok

For checking that stuff is super-ok across a group of git repositories.

## Really?

Well, it doesn't confirm ok-ness so much as report back useful information that can help you decide the state of things.

## how?

Install the super-ok app folder alongside repositories you want to check. From inside super-ok run `node index.js` and watch the magic happen.

### what happens?

super-ok runs through sibling repos and reports what branch they're on.

Run super-ok with the `--tags` command it'll check if sibling repos have tags and if those tags are prefixed with a v.

### want to go global?

Download the code. Run `npm install -g` and you can run the command `ok` to run super-ok.
